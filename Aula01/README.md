# Aula 01 - VSCode e Manipulação de Arquivos

Esta aula é pra que vocês se acostumem a manipular arquivos no computador, são coisas básicas como criar pastas, salvar arquivos, editar arquivos usando o VSCode, etc...

1. Baixe o VSCode
2. Criar uma conta no Gitlab.
3. Criar uma pastinha no computador chamada: Curso Programação Web
4. Criar uma pastinha chamada Aula01
5. Abrir o VSCode
6. Clique no menu Arquivo e clique no item em Abrir
7. Procure a pasta Aula01 e clique em Abrir
8. Clique novamente no menu Arquivo e clique em Novo arquivo
9. No novo arquivo, na primeira linha, digite o seguinte: `<h1>Minha Primeira Página</h1>`
10. Clique no menu Arquivo novamente e clique em Salvar
11. Certifique-se de que o arquivo vai ser salvo na pasta Aula01, dê o nome de: index.html
12. Depois de salvar, saia do editor (não feche, só minimize) e vá para o Windows Explorer (ou Finder se estiver no Mac)
13. Vá até a pasta Aula01, o arquivo index.html deve estar lá. Dê dois cliques para abri-lo
14. O arquivo provavelmente vai ser aberto no navegador padrão (Chrome, Internet Explorer, Safari, etc...)
15. Caso o arquivo não seja aberto no navegador padrão, clique com o botão direito, e selecione a opção 'Abrir com' e selecione o navegador
16. Volte ao VSCode, na linha abaixo escreva o seguinte: `<h2>Sou ninja</h2>`
17. Clique em Arquivo / Salvar, ou Control + S
18. Volte ao navegador e recarregue a página
19. A página deve exibir duas linhas, a primeira bem grande, a segunda um pouco menor, com as frases: "Minha Primeira Página" e "Sou ninja"
20. Parabéns, você já sabe o mínimo para ser um programador.

Com o tempo essas atividades de criar arquivos deve se tornar natural, vocês vão fazer sem pensar, mas agora no início é bom que a gente fale a mesma língua.
