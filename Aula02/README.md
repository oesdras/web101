# Aula 02 - Mais VSCode e HTML Básico

Esta aula tem como objetivo explorar um pouco o VSCode bem como expandir o conhecimento de vocês sobre HTML.

1. Clique no Menu Arquivo e clique em Abrir
2. Selecione a pasta "Curso Programação Web" e clique em Abrir
3. Observe ao lado esquerdo (File Explorer) que agora a pasta Aula01 está alí
4. Crie uma nova pasta `Aula02` alí no lado esquerdo, há várias formas de fazer isso
   1. Veja que tem o ícone de uma pastinha com um + em cima, podem clicar alí
   2. Clique com o botão direito no painel (em uma área vazia) e clique em Nova pasta
5. Crie outro arquivo `index.html` dentro da pasta `Aula02`:
   1. Clique com o botão direito sobre a pasta `Aula02` e selecione `Novo arquivo`
   2. Digite o nome do arquivo: `index.html` e precione a tecla `Enter`
   3. Clique sobre o arquivo para abrilo no editor de texto.
6. Neste arquivo a gente vai explorar as seguintes tags:
   1. `html` - Marca o início e fim do documento HTML
   2. `head` - Marca o início e fim da cabeça do documento, esta parte não é exibida no navegador
   3. `title` - Marca o título do documento, este título vai ser exibido pelo navegador na barra de título e/ou na aba.
   4. `body` - Marca o início e fim do corpo do documento, tudo o que estiver dentro de `<body></body>` vai ser exibido na página.
   5. `h1`, `h2`, `h3`, ... `h6` - Marcam o início e fim de cabeçalhos, títulos de seções, etc...
   6. `ol` - Marcam o início e fim de listas ordenadas (que tem números ao invés de bolinhas)
   7. `ul` - Marcam o início e fim de lista não ordenadas (bolinhas ao invés de números)
   8. `li` - Marcam o início e fim de um ítem de uma lista
   9. `table` - Marcam o início e fim de uma tabela
   10. `tr` - Marcam o início e fim de uma linha de tabela
   11. `td` - Marcam o início e fim de uma célula de uma tabela
   12. `thead` - Marcam o início e fim da cabeça da tabela
   13. `tbody` - Marcam o início e fim do corpo da tabela
   14. `tfoot` - Marcam o início e fim do rodapé da tabela
   15. `caption` - Marcam o início e fim da legenda/título da tabela
7. Para isso, copie o código abaixo e cole no arquivo `index.html`, salve o arquivo e abra-o no navegador.

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Minha Segunda Página</title>
  </head>
  <body>
    <!--
      EU SOU UM COMENTÁRIO, O NAVEGADOR VAI ME IGNORAR E NADA VAI SER EXIBIDO NA TELA
    -->
    <h1>Eu sou um cabeçalho (header) de primeiro nível</h1>
    <h2>Eu sou um cabeçalho de segundo nível</h2>
    <h3>Eu sou um cabeçalho de terceiro nível</h3>
    <h4>Eu sou um cabeçalho de quarto nível</h4>
    <h5>Eu sou um cabeçalho de quinto nível</h5>
    <h6>Eu sou um cabeçalho de sexto nível</h6>

    <p>Eu sou um parágrafo, sou muito usado para blocos de texto</p>
    <h3>
      Abaixo vamos explorar algumas das principais tags usadas em documentos
      HTML
    </h3>

    <h4>Lista Ordenada:</h4>
    <ol>
      <li>Item 1</li>
      <li>Item 2</li>
      <li>Item 3</li>
    </ol>
    <p>
      Observe que a gente usa as a tag <code>ol</code> para definir a lista, e a
      tag <code>li</code> para definir cada ítem da lista. <code>ol</code> é
      abreviação de Ordered List.
    </p>

    <h4>Lista Não Ordenada:</h4>

    <ul>
      <li>Item 1</li>
      <li>Item 2</li>
      <li>Item 3</li>
    </ul>

    <p>
      Observe que a gente usa as a tag <code>ul</code> para definir a lista, e a
      tag <code>li</code> para definir cada ítem da lista. <code>ul</code> é
      abreviação de Unordered List.
    </p>

    <h4>Tabela</h4>
    <table>
      <caption>
        Tags usadas pra criar uma tabela
      </caption>
      <thead>
        <tr>
          <th>Nome</th>
          <th>Propósito</th>
          <th>Opcional</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><code>table</code></td>
          <td>Marca o início e fim da tabela</td>
          <td>Não</td>
        </tr>
        <tr>
          <td><code>tr</code></td>
          <td>Marca o início e fim de uma linha da tabela</td>
          <td>Não</td>
        </tr>
        <tr>
          <td><code>td</code></td>
          <td>Marca o início e fim de uma célula da tabela</td>
          <td>Não</td>
        </tr>
        <tr>
          <td><code>thead</code></td>
          <td>Marca o início e fim do cabeçalho da tabela</td>
          <td>Sim</td>
        </tr>
        <tr>
          <td><code>tbody</code></td>
          <td>Marca o início e fim do corpo da tabela</td>
          <td>Sim</td>
        </tr>
        <tr>
          <td><code>tbody</code></td>
          <td>Marca o início e fim do rodapé da tabela</td>
          <td>Sim</td>
        </tr>
        <tr>
          <td><code>caption</code></td>
          <td>Marca o início e fim da legenda/título da tabela</td>
          <td>Sim</td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="2">
            É possível criar tabelas só com <code>table</code>, <code>tr</code>,
            e <code>td</code>. As outras tags são opcionais e servem só para
            deixar a tabela mais rica.
          </td>
        </tr>
      </tfoot>
    </table>
  </body>
</html>
```

8. Observe o como o navegador desenha (renderiza) cada elemento na página, dessa forma você vai ter uma ideia do propósito de cada elemento. Os elementos estão sem estilo nenhum, nas próximas aulas a gente vai deixa-los mais bonitos usando outra linguagem: O `CSS`.

## Exercício

1. Crie um arquivo na pasta `Aula02` e de o nome de `receita.html`, utilize o código abaixo para começar

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Bolo Fácil de Liquidificador</title>
  </head>
  <body>
    <h1>Bolo Fácil de Liquidificador</h1>

    <!-- Escreva a receita aqui embaixo -->
  </body>
</html>
```

2. Digite a receita de bolo que está [nesta página](http://gshow.globo.com/receitas-gshow/receita/bolo-facil-de-liquidificador-524b51764d38852aba00004c.html)
3. Não esqueça de usar as tags de cabeçalho, lista e parágrafos de acordo.

Boa sorte!
